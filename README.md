# 42Race Yelp Fusion App for Android

42Race Yelp Fusion App is an Android app that would fetch and display results of local business from [Yelp Fusion API](https://www.yelp.com/developers/documentation/v3).

## Features
- Search local business by name, location, and category
- Sort local businesses by recommended (best match), distance, or rating
- View local business details

## Tech Stack

This project uses monolith architecture with MVVM

## Development setup

This project developed using Android Studio Bumblebee | 2021.1.1 Beta 4

### API

Using [Yelp Fusion API](https://www.yelp.com/developers/documentation/v3) to fetch local business.

### Libraries

- Application entirely written in [Kotlin](https://kotlinlang.org)
- Asynchronous processing using [Coroutines](https://kotlin.github.io/kotlinx.coroutines/)
- Uses [Koin](https://github.com/InsertKoinIO/koin) for dependency injection
- Uses [Jetpack Navigation](https://developer.android.com/guide/navigation) for navigation between fragment
- Uses [Jetpack Paging](https://developer.android.com/jetpack/androidx/releases/paging) to load data gradually
- Uses [Moshi](https://github.com/square/moshi) for parsing JSON object
- Uses [Glide](https://github.com/bumptech/glide) for loading image

## Creator
This application is fully created by [Gilar Purwita Subagja](https://www.linkedin.com/in/gilarps/)