@file:Suppress("unused")

package com.gilar.a42race.yelp

import android.app.Application
import com.gilar.a42race.yelp.di.networkModule
import com.gilar.a42race.yelp.di.repositoryModule
import com.gilar.a42race.yelp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(viewModelModule)
            modules(networkModule)
            modules(repositoryModule)
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
