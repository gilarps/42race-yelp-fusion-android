package com.gilar.a42race.yelp.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Business(

	@Json(name="distance")
	val distance: Double? = null,

	@Json(name="image_url")
	val imageUrl: String? = null,

	@Json(name="rating")
	val rating: Float? = null,

	@Json(name="coordinates")
	val coordinates: Coordinate? = null,

	@Json(name="review_count")
	val reviewCount: Int? = null,

	@Json(name="transactions")
	val transactions: List<String?>? = null,

	@Json(name="photos")
	val photos: List<String?>? = null,

	@Json(name="url")
	val url: String? = null,

	@Json(name="phone")
	val phone: String? = null,

	@Json(name="display_phone")
	val displayPhone: String? = null,

	@Json(name="price")
	val price: String? = null,

	@Json(name="name")
	val name: String? = null,

	@Json(name="alias")
	val alias: String? = null,

	@Json(name="location")
	val location: Location? = null,

	@Json(name="id")
	val id: String? = null,

	@Json(name="categories")
	val categories: List<Category> = ArrayList(),

	@Json(name="is_closed")
	val isClosed: Boolean? = null
) : Parcelable {
	val distanceInKm = distance?.div(1000.0)
	fun displayAddress() : String {
		var address = ""
		if (location?.displayAddress != null) {
			for (i in location.displayAddress.indices) {
				if (i > 0) {
					address += "\n"
				}
				address += location.displayAddress[i]
			}
		} else {
			address = location?.address1 + " " + location?.city + " " + location?.country
		}
		return address
	}
}