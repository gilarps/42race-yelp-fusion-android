package com.gilar.a42race.yelp.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BusinessSearchResponse(

	@Json(name="total")
	val total: Int? = null,

	@Json(name="region")
	val region: Region? = null,

	@Json(name="businesses")
	val businesses: List<Business> = ArrayList()
)