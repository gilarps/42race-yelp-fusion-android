package com.gilar.a42race.yelp.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CategoriesResponse(

    @Json(name="categories")
	val categories: List<Category> = ArrayList(),
)