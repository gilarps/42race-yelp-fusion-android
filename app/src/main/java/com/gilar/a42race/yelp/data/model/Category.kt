package com.gilar.a42race.yelp.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Category(

	@Json(name="alias")
	val alias: String? = null,

	@Json(name="title")
	val title: String? = null,

	val icon: Int?

) : Parcelable