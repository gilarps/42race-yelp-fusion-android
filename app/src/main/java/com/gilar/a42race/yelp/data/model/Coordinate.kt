package com.gilar.a42race.yelp.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Coordinate(

	@Json(name="latitude")
	val latitude: Double? = null,

	@Json(name="longitude")
	val longitude: Double? = null
) : Parcelable