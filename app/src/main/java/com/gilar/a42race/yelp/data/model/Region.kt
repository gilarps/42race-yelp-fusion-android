package com.gilar.a42race.yelp.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Region(

	@Json(name="center")
	val center: Coordinate? = null

)