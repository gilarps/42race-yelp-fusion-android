package com.gilar.a42race.yelp.data.remote

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.gilar.a42race.yelp.data.model.BusinessSearchPagingSource
import com.gilar.a42race.yelp.data.model.Business
import kotlinx.coroutines.flow.Flow

class YelpFusionRepository(private val service: YelpFusionService) {

    fun getBusiness(category: String? = null, businessName: String? = null, location: String? = null, sortBy: String): Flow<PagingData<Business>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = NETWORK_PAGE_SIZE),
            pagingSourceFactory = { BusinessSearchPagingSource(service, category, businessName, location, sortBy) }
        ).flow
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 8
    }
}
