package com.gilar.a42race.yelp.data.remote

import android.content.Context
import com.gilar.a42race.yelp.BuildConfig.YELP_BASE_URL
import com.gilar.a42race.yelp.data.model.BusinessSearchResponse
import com.gilar.a42race.yelp.util.SORT_MODE_BEST_MATCH
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface YelpFusionService {

    @GET("businesses/search")
    suspend fun fetchBusinessSearch(
        @Query("location") location: String? = "San Francisco, CA",
        @Query("offset") offset: Int? = 0,
        @Query("limit") limit: Int? = 20,
        @Query("sort_by") sortBy: String? = SORT_MODE_BEST_MATCH,
        @Query("categories") categories: String? = null,
        @Query("term") term: String? = null,
    ): BusinessSearchResponse

    companion object {

        fun create(context: Context): YelpFusionService {
            val logger =
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

            val cacheSize = (5 * 1024 * 1024).toLong()
            val myCache = Cache(context.cacheDir, cacheSize)

            val client = OkHttpClient.Builder()
                .cache(myCache)
                .addInterceptor(logger)
                .addInterceptor(YelpFusionInterceptor())
                .build()

            return Retrofit.Builder()
                .baseUrl(YELP_BASE_URL)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .build()
                .create(YelpFusionService::class.java)
        }
    }
}
