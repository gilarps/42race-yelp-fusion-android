package com.gilar.a42race.yelp.di

import com.gilar.a42race.yelp.data.remote.YelpFusionService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val networkModule = module {

    single {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single { YelpFusionService.create(androidContext()) }

}
