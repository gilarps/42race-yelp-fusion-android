package com.gilar.a42race.yelp.di

import com.gilar.a42race.yelp.data.remote.YelpFusionRepository
import org.koin.dsl.module

val repositoryModule = module {

    single { YelpFusionRepository(get()) }

}
