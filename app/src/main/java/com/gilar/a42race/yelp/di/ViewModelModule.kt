package com.gilar.a42race.yelp.di

import com.gilar.a42race.yelp.presentation.ui.detail.DetailViewModel
import com.gilar.a42race.yelp.presentation.ui.home.HomeViewModel
import com.gilar.a42race.yelp.presentation.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        HomeViewModel(
        )
    }
    viewModel {
        SearchViewModel(
            get()
        )
    }
    viewModel {
        DetailViewModel(
        )
    }
}
