package com.gilar.a42race.yelp.extension

import android.graphics.Typeface
import android.widget.TextView

fun TextView.setStyleBold() {
    this.setTypeface(this.typeface, Typeface.BOLD)
}

fun TextView.setStyleNormal() {
    this.setTypeface(this.typeface, Typeface.NORMAL)
}