package com.gilar.a42race.yelp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.gilar.a42race.yelp.R
import com.gilar.a42race.yelp.data.model.Business
import com.gilar.a42race.yelp.databinding.ItemBusinessBinding
import com.gilar.a42race.yelp.presentation.ui.search.SearchFragment

/**
 * Adapter for the [RecyclerView] in [SearchFragment].
 */

@BindingAdapter("imageFromUrl")
fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(imageUrl)
            .apply(RequestOptions().placeholder(R.color.grey).centerCrop())
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

class BusinessAdapter(private val onClick: (Business) -> Unit) :
    PagingDataAdapter<Business, BusinessAdapter.BusinessViewHolder>(GalleryDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessViewHolder {
        return BusinessViewHolder(
            ItemBusinessBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClick
        )
    }

    override fun onBindViewHolder(holder: BusinessViewHolder, position: Int) {
        val business = getItem(position)
        if (business != null) {
            holder.bind(business)
        }
    }

    class BusinessViewHolder(
        private val binding: ItemBusinessBinding,
        val onClick: (Business) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.business?.let { business ->
                    onClick(business)
                }
            }
        }

        fun bind(item: Business) {
            val categoryTags: MutableList<String> = ArrayList()
            for (category in item.categories) {
                categoryTags.add(category.title.toString())
            }
            binding.categoriesTagGroup.setTags(categoryTags)
            binding.apply {
                business = item
                executePendingBindings()
            }
        }
    }
}

private class GalleryDiffCallback : DiffUtil.ItemCallback<Business>() {
    override fun areItemsTheSame(oldItem: Business, newItem: Business): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Business, newItem: Business): Boolean {
        return oldItem == newItem
    }
}
