package com.gilar.a42race.yelp.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gilar.a42race.yelp.data.model.Category
import com.gilar.a42race.yelp.databinding.ItemCategoryBinding
import com.gilar.a42race.yelp.presentation.ui.search.SearchFragment

/**
 * Adapter for the [RecyclerView] in [SearchFragment].
 */

@BindingAdapter("imageFromDrawable")
fun bindImageFromDrawable(view: ImageView, drawable: Int?) {
    Glide.with(view.context)
        .load(drawable)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(view)
}

class CategoryAdapter(
    private var categories: List<Category>, private val onClick: (Category) -> Unit
) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClick
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.bind(category)
    }

    class CategoryViewHolder(
        private val binding: ItemCategoryBinding,
        val onClick: (Category) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.category?.let { category ->
                    onClick(category)
                }
            }
        }

        fun bind(item: Category) {
            binding.apply {
                category = item
                executePendingBindings()
            }
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }
}