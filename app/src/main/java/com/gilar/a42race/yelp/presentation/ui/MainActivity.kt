package com.gilar.a42race.yelp.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gilar.a42race.yelp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}