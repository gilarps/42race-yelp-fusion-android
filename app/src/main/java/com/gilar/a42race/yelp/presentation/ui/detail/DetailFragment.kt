package com.gilar.a42race.yelp.presentation.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.gilar.a42race.yelp.R
import com.gilar.a42race.yelp.base.BaseFragment
import com.gilar.a42race.yelp.databinding.FragmentDetailBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailFragment : BaseFragment<FragmentDetailBinding, DetailViewModel>() {

    private val viewModel: DetailViewModel by viewModel()
    override fun createViewModel() = viewModel

    private val args: DetailFragmentArgs by navArgs()

    override fun getViewBinding(): FragmentDetailBinding {
        return FragmentDetailBinding.inflate(layoutInflater)
    }

    override fun onFirstLaunch(savedInstanceState: Bundle?, view: View) {
        setupActionBar()
        mViewBinding.business = args.business
        val categoryTags: MutableList<String> = ArrayList()
        for (category in args.business.categories) {
            categoryTags.add(category.title.toString())
        }
        mViewBinding.categoriesTagGroup.setTags(categoryTags)
    }

    private fun setupActionBar() {
        mViewBinding.run {
            // Add back button
            toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_ios_new_24)
            toolbar.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    override fun initUiListener() {
        mViewBinding.clickDirectionsListener = View.OnClickListener {
            val url =
                "https://www.google.com/maps/search/?api=1&query=${args.business.coordinates?.latitude}%2C${args.business.coordinates?.longitude}"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        mViewBinding.clickCallListener = View.OnClickListener {
            val i = Intent(Intent.ACTION_DIAL)
            i.data = Uri.parse("tel:" + args.business.phone.toString())
            startActivity(i)
        }
    }
}