package com.gilar.a42race.yelp.presentation.ui.home

import android.os.Bundle
import android.view.View
import com.gilar.a42race.yelp.base.BaseFragment
import com.gilar.a42race.yelp.data.model.Category
import com.gilar.a42race.yelp.databinding.FragmentHomeBinding
import com.gilar.a42race.yelp.presentation.adapter.CategoryAdapter
import com.gilar.a42race.yelp.util.CATEGORIES
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    private val viewModel: HomeViewModel by viewModel()
    override fun createViewModel() = viewModel

    private var categories: List<Category> = CATEGORIES
    private val adapter = CategoryAdapter(categories) { category ->
        val action = HomeFragmentDirections.actionHomeFragmentToSearchFragment(category)
        getNavController()?.navigate(action)
    }

    override fun getViewBinding(): FragmentHomeBinding {
        return FragmentHomeBinding.inflate(layoutInflater)
    }

    override fun onFirstLaunch(savedInstanceState: Bundle?, view: View) {
        mViewBinding.recyclerView.adapter = adapter
    }

    override fun initUiListener() {
        mViewBinding.cardSearch.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToSearchFragment(null)
            getNavController()?.navigate(action)
        }
    }

}