package com.gilar.a42race.yelp.presentation.ui.search

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.gilar.a42race.yelp.R
import com.gilar.a42race.yelp.base.BaseFragment
import com.gilar.a42race.yelp.data.model.Category
import com.gilar.a42race.yelp.databinding.BottomSheetSortSelectBinding
import com.gilar.a42race.yelp.databinding.FragmentSearchBinding
import com.gilar.a42race.yelp.extension.setStyleBold
import com.gilar.a42race.yelp.extension.setStyleNormal
import com.gilar.a42race.yelp.presentation.adapter.BusinessAdapter
import com.gilar.a42race.yelp.presentation.adapter.CategoryAdapter
import com.gilar.a42race.yelp.presentation.adapter.LoadStateAdapter
import com.gilar.a42race.yelp.util.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class SearchFragment : BaseFragment<FragmentSearchBinding, SearchViewModel>() {

    private val viewModel: SearchViewModel by viewModel()
    override fun createViewModel() = viewModel

    private val args: SearchFragmentArgs by navArgs()
    private var searchJob: Job? = null
    private var isCategorySelected = false
    private var category: Category? = null
    private var sortBy = SORT_MODE_BEST_MATCH

    private val adapter = BusinessAdapter { business ->
        val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment(business)
        getNavController()?.navigate(action)
    }

    override fun getViewBinding(): FragmentSearchBinding {
        return FragmentSearchBinding.inflate(layoutInflater)
    }

    override fun onFirstLaunch(savedInstanceState: Bundle?, view: View) {
        category = args.category
        // Set search location default value
        mViewBinding.etSearchLocation.setText(SEARCH_LOCATION_DEFAULT)
        // Setup local business RecyclerView
        mViewBinding.recyclerViewBusiness.adapter = adapter.withLoadStateHeaderAndFooter(
            header = LoadStateAdapter { adapter.retry() },
            footer = LoadStateAdapter { adapter.retry() }
        )
        // Setup category RecyclerView
        mViewBinding.recyclerViewCategory.adapter = CategoryAdapter(CATEGORIES) {
            isCategorySelected = true
            category = it
            mViewBinding.etSearchBusinessName.setText(it.title)
            getBusiness(
                category = category?.alias,
                businessLocation = mViewBinding.etSearchLocation.text.toString()
            )
        }
        if (category != null) {
            // category is not null
            // means that user select category before
            // and app should set search business name text to category title
            // then get business from server
            isCategorySelected = true
            mViewBinding.recyclerViewCategory.isVisible = false
            mViewBinding.etSearchBusinessName.setText(category?.title)
            getBusiness(
                category = category?.alias,
                businessLocation = mViewBinding.etSearchLocation.text.toString()
            )
        } else {
            // category is null
            // means that user did not select category before
            // and app should show category list
            isCategorySelected = false
            mViewBinding.recyclerViewCategory.isVisible = true
        }
    }

    private fun getBusiness(
        category: String? = null,
        businessName: String? = null,
        businessLocation: String? = null
    ) {
        // Make sure we cancel the previous job before creating a new one
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.getBusiness(category, businessName, businessLocation, sortBy).collectLatest {
                adapter.submitData(it)
            }
        }
    }

    /**
     * Render UI when adapter load state change
     * */
    private fun renderUi(loadState: CombinedLoadStates) {
        // Only shows the list, sort button, and separator if refresh succeeds
        mViewBinding.recyclerViewBusiness.isVisible =
            loadState.source.refresh is LoadState.NotLoading
        mViewBinding.btnSort.isVisible = loadState.source.refresh is LoadState.NotLoading
        mViewBinding.separator.isVisible = loadState.source.refresh is LoadState.NotLoading
        // Show loading spinner during initial load or refresh
        mViewBinding.progressIndicator.isVisible = loadState.source.refresh is LoadState.Loading
        // Show the retry state if initial load or refresh fails
        mViewBinding.btnRetry.isVisible = loadState.source.refresh is LoadState.Error

        // Flag to check whether list is empty
        val isListEmpty = loadState.refresh is LoadState.NotLoading && adapter.itemCount == 0

        // If list is empty or load state is error or load state is loading,
        // show text message
        mViewBinding.tvMessage.isVisible = isListEmpty
                || loadState.source.refresh is LoadState.Error
                || loadState.source.refresh is LoadState.Loading

        // Set text message that will appear
        when (loadState.source.refresh) {
            is LoadState.Error -> {
                // Error State
                Timber.e("Loading error")
                when ((loadState.source.refresh as LoadState.Error).error) {
                    is UnknownHostException,
                    is SocketTimeoutException -> {
                        // Show connection error message
                        mViewBinding.tvMessage.text =
                            getString(R.string.failed_to_connect_to_the_server_check_internet_connection)
                    }
                    else -> {
                        // Show general error message
                        mViewBinding.tvMessage.text = getString(R.string.failed_to_get_data)
                    }
                }
            }
            is LoadState.Loading -> {
                // Loading State
                // Show loading message
                Timber.i("Loading data...")
                mViewBinding.tvMessage.text = getString(R.string.please_wait)
            }
            is LoadState.NotLoading -> {
                Timber.i("Not loading data...")
            }
            else -> {
                if (isListEmpty) {
                    // Show empty message
                    Timber.w("List is empty")
                    mViewBinding.tvMessage.text =
                        getString(R.string.data_not_found)
                }
            }
        }
    }

    override fun initUiListener() {
        // Make adapter can listen loading state
        adapter.addLoadStateListener { loadState -> renderUi(loadState) }
        mViewBinding.etSearchBusinessName.addTextChangedListener {
            // search business name EditText value changed
            // app should show category list if EditText value is empty
            mViewBinding.recyclerViewCategory.isVisible = it.toString().isEmpty()
            // set category to null
            isCategorySelected = false
            category = null
        }
        mViewBinding.buttonSearch.setOnClickListener {
            // search button clicked
            if (mViewBinding.etSearchBusinessName.text.toString().isEmpty()) {
                // if business name search EditText is empty,
                // app should request focus business name search EditText
                mViewBinding.etSearchBusinessName.requestFocus()
            } else {
                // if business name search EditText is not empty,
                // app should get business from server
                getBusiness(
                    category = category?.alias,
                    businessLocation = mViewBinding.etSearchLocation.text.toString(),
                    businessName = mViewBinding.etSearchBusinessName.text.toString()
                )
            }
        }
        mViewBinding.btnRetry.setOnClickListener {
            // retry button clicked
            getBusiness(
                category = category?.alias,
                businessLocation = mViewBinding.etSearchLocation.text.toString(),
                businessName = mViewBinding.etSearchBusinessName.text.toString()
            )
        }
        mViewBinding.btnSort.setOnClickListener {
            // sort button clicked
            // app should show sort BottomSheetDialog
            val dialogSort =
                BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTopRounded)
            dialogSort.dismissWithAnimation = true
            val dialogSortBinding =
                BottomSheetSortSelectBinding.inflate(layoutInflater, null, false)
            dialogSort.setContentView(dialogSortBinding.root)
            // check current sort value
            // set which text to bold based on current sort value
            when (sortBy) {
                SORT_MODE_BEST_MATCH -> {
                    dialogSortBinding.tvBestMatch.setStyleBold()
                    dialogSortBinding.tvDistance.setStyleNormal()
                    dialogSortBinding.tvRating.setStyleNormal()
                }
                SORT_MODE_DISTANCE -> {
                    dialogSortBinding.tvBestMatch.setStyleNormal()
                    dialogSortBinding.tvDistance.setStyleBold()
                    dialogSortBinding.tvRating.setStyleNormal()
                }
                SORT_MODE_RATING -> {
                    dialogSortBinding.tvBestMatch.setStyleNormal()
                    dialogSortBinding.tvDistance.setStyleNormal()
                    dialogSortBinding.tvRating.setStyleBold()
                }
            }
            dialogSortBinding.tvBestMatch.setOnClickListener {
                // best match (recommended) selected
                sortBy = SORT_MODE_BEST_MATCH
                (getString(R.string.sort) + " " + getString(R.string.recommended_default)).also {
                    mViewBinding.btnSort.text = it
                }
                dialogSort.dismiss()
                getBusiness(
                    category = category?.alias,
                    businessLocation = mViewBinding.etSearchLocation.text.toString(),
                    businessName = mViewBinding.etSearchBusinessName.text.toString()
                )

            }
            dialogSortBinding.tvDistance.setOnClickListener {
                // distance selected
                sortBy = SORT_MODE_DISTANCE
                (getString(R.string.sort) + " " + getString(R.string.distance)).also {
                    mViewBinding.btnSort.text = it
                }
                dialogSort.dismiss()
                getBusiness(
                    category = category?.alias,
                    businessLocation = mViewBinding.etSearchLocation.text.toString(),
                    businessName = mViewBinding.etSearchBusinessName.text.toString()
                )
            }
            dialogSortBinding.tvRating.setOnClickListener {
                // rating selected
                sortBy = SORT_MODE_RATING
                (getString(R.string.sort) + " " + getString(R.string.rating)).also {
                    mViewBinding.btnSort.text = it
                }
                dialogSort.dismiss()
                getBusiness(
                    category = category?.alias,
                    businessLocation = mViewBinding.etSearchLocation.text.toString(),
                    businessName = mViewBinding.etSearchBusinessName.text.toString()
                )
            }
            dialogSort.show()
        }
    }

}