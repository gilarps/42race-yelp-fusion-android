package com.gilar.a42race.yelp.presentation.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.gilar.a42race.yelp.data.model.Business
import com.gilar.a42race.yelp.data.remote.YelpFusionRepository
import com.gilar.a42race.yelp.util.SORT_MODE_BEST_MATCH
import kotlinx.coroutines.flow.Flow

class SearchViewModel(
    private val repository: YelpFusionRepository
    ) : ViewModel() {

    /**
     * Get local business from server using pagination
     * */
    fun getBusiness(category: String? = null, businessName: String? = null, location: String? = null, sortBy: String = SORT_MODE_BEST_MATCH): Flow<PagingData<Business>> {
        return repository.getBusiness(category, businessName, location, sortBy).cachedIn(viewModelScope)
    }

}