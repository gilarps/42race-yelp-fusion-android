package com.gilar.a42race.yelp.util

import com.gilar.a42race.yelp.R
import com.gilar.a42race.yelp.data.model.Category


/**
 * Constants used throughout the app
 */

const val SEARCH_LOCATION_DEFAULT = "Singapore"

const val SORT_MODE_BEST_MATCH = "best_match"
const val SORT_MODE_DISTANCE = "distance"
const val SORT_MODE_RATING = "rating"

val CATEGORIES: List<Category> = listOf(
    Category("restaurants", "Restaurants", R.drawable.ic_category_restaurants),
    Category("cafes", "Cafe", R.drawable.ic_category_cafes),
    Category("nightlife", "Nightlife", R.drawable.ic_category_nightlife),
    Category("shopping", "Shopping", R.drawable.ic_category_shopping),
    Category("hotels", "Hotels", R.drawable.ic_category_hotels),
    Category("health", "Health", R.drawable.ic_category_hospitals),
)